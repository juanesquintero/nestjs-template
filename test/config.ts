import * as dotenv from 'dotenv';
dotenv.config();

export const accessToken = process.env.JWT_TEST_TOKEN;
export const auth = {
  k: 'Authorization',
  v: 'Bearer ' + accessToken,
};
