import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { CoursesModule } from '../src/courses/courses.module';
import { Course } from './../src/courses/entities/courses.entity';
import { classMock } from './../src/shared/utils';
import { CoursesService } from './../src/courses/courses.service';
import { ROUTES } from './../src/shared/constants';
import { auth } from './config';

const mock: Course = {
  id: 'courseID',
  title: 'Course Title',
  description: 'Course Description',
  hours: 10,
  price: 100.0,
};
const { id, ...mockDTO } = mock;
const service = classMock(CoursesService, mock);
service.remove = jest.fn().mockResolvedValue({ delete: true });

describe('CourseController (e2e)', () => {
  let app: INestApplication;
  const endpoint = `/${ROUTES.courses}`;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, CoursesModule],
    })
      .overrideProvider(CoursesService)
      .useValue(service)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it(`${endpoint} (GET)`, async () => {
    const expected = await service.findAll();
    return request(app.getHttpServer())
      .get(endpoint)
      .expect(200)
      .expect(expected);
  });

  it(`${endpoint}/:id (GET)`, async () => {
    const expected = await service.findOne(mock.id);
    return request(app.getHttpServer())
      .get(`${endpoint}/${id}`)
      .expect(200)
      .expect(expected);
  });

  it(`${endpoint} (POST)`, async () => {
    const expected = await service.create(mockDTO);
    return request(app.getHttpServer())
      .post(endpoint)
      .set(auth.k, auth.v)
      .send(mockDTO)
      .expect(201)
      .expect(expected);
  });

  it(`${endpoint} (PUT)`, async () => {
    const expected = await service.update(id, mockDTO);
    return request(app.getHttpServer())
      .put(`${endpoint}/${id}`)
      .set(auth.k, auth.v)
      .send(mockDTO)
      .expect(201)
      .expect(expected);
  });

  it(`${endpoint} (DELETE)`, async () => {
    await service.remove(id);
    return request(app.getHttpServer())
      .delete(`${endpoint}/${id}`)
      .set(auth.k, auth.v)
      .expect(204)
      .expect({});
  });
});
