import { loginMock, userMock } from '../src/mocks/user.mock';
import { AuthModule } from './../src/auth/auth.module';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { servicerMock } from './../src/shared/utils';
import { AuthService } from './../src/auth/auth.service';
import { ROUTES } from './../src/shared/constants';

const service = servicerMock(AuthService, loginMock);
service.registerUser = jest.fn().mockResolvedValue(userMock);

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  const endpoint = `/${ROUTES.auth}`;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, AuthModule],
    })
      .overrideProvider(AuthService)
      .useValue(service)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it(`${endpoint}/login (POST)`, async () => {
    const expected = await service.loginUser(loginMock);
    return request(app.getHttpServer())
      .post(`${endpoint}/login`)
      .send(loginMock)
      .expect(200)
      .expect(expected);
  });

  it(`${endpoint}/singup (POST)`, async () => {
    const expected = await service.registerUser(userMock);
    return request(app.getHttpServer())
      .post(`${endpoint}/singup`)
      .send(userMock)
      .expect(201)
      .expect(expected);
  });

  // it(`${endpoint}/me (GET)`, async () => {
  //   const expected = await service.me(jwt);
  //   return request(app.getHttpServer())
  //     .get(`${endpoint}/me`)
  //     .auth(jwt)
  //     .expect(200)
  //     .expect(expected);
  // });
});
