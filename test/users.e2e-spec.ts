import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { servicerMock } from './../src/shared/utils';
import { UsersService } from './../src/users/users.service';
import { AppModule } from '../src/app.module';
import { UsersModule } from '../src/users/users.module';
import { ROUTES } from '../src/shared/constants';
import { userMock, userDTOMock } from './../src/mocks/user.mock';
import { auth } from './config';

const mock = userMock;
const mockDTO = userDTOMock;

const service = servicerMock(UsersService, mock);

describe('UserController (e2e)', () => {
  let app: INestApplication;
  const endpoint = `/${ROUTES.users}`;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UsersModule],
    })
      .overrideProvider(UsersService)
      .useValue(service)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it(`${endpoint} (GET)`, async () => {
    const expected = await service.findAll();
    return request(app.getHttpServer())
      .get(endpoint)
      .set(auth.k, auth.v)
      .expect(200)
      .expect(expected);
  });

  it(`${endpoint}/:email (GET)`, async () => {
    const expected = await service.findOne(mock.email);
    return request(app.getHttpServer())
      .get(`${endpoint}/${mock.email}`)
      .set(auth.k, auth.v)
      .expect(200)
      .expect(expected);
  });

  it(`${endpoint} (POST)`, async () => {
    const expected = await service.create(mockDTO);
    return request(app.getHttpServer())
      .post(endpoint)
      .set(auth.k, auth.v)
      .send(mockDTO)
      .expect(201)
      .expect(expected);
  });

  it(`${endpoint} (PUT)`, async () => {
    const expected = await service.update(mock.email, mockDTO);
    return request(app.getHttpServer())
      .put(`${endpoint}/${mock.email}`)
      .set(auth.k, auth.v)
      .send(mockDTO)
      .expect(201)
      .expect(expected);
  });

  it(`${endpoint} (DELETE)`, async () => {
    await service.remove(mock.email);
    return request(app.getHttpServer())
      .delete(`${endpoint}/${mock.email}`)
      .set(auth.k, auth.v)
      .expect(204)
      .expect({});
  });
});
