-- table users
CREATE TABLE users (
  name VARCHAR(250) NOT NULL UNIQUE,
  email VARCHAR(250) NOT NULL,
  password VARCHAR(100) NOT NULL,
  status VARCHAR(20) NOT NULL,
  PRIMARY KEY (email)
);

-- table courses
CREATE TABLE courses (
  id VARCHAR(50) NOT NULL,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(200) NULL,
  hours INT NOT NULL,
  price DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (id)
);
