INSERT INTO users ("email", "name", "password", "status") VALUES 
('wanda_olsen@gmail.com','Wanda Olsen','$2b$10$NFH21fizDQtDY.s/tjaQv.4crSXn6uwgR0LqdwGMHakcWDeLrDlca', 'active'),
('sharron_sargent@hotmail.com','Sharron Sargent','$2b$10$NFH21fizDQtDY.s/tjaQv.4crSXn6uwgR0LqdwGMHakcWDeLrDlca', 'active'),
('huff_dunlap@gmail.com','Huff Dunlap','$2b$10$NFH21fizDQtDY.s/tjaQv.4crSXn6uwgR0LqdwGMHakcWDeLrDlca', 'active'),
('juliet_contreras@outlook.com','Juliet Contreras','$2b$10$NFH21fizDQtDY.s/tjaQv.4crSXn6uwgR0LqdwGMHakcWDeLrDlca', 'inactive'),
('mary_horne@yahoo.com','Mary Horne','$2b$10$NFH21fizDQtDY.s/tjaQv.4crSXn6uwgR0LqdwGMHakcWDeLrDlca', 'inactive');

INSERT INTO courses ("id", "title", "description", "hours", "price") VALUES ('61d21fdc64a21a99efe0d8f7','Intro to JavaScript','...', 15, 49.9),
('61d21fdce151182e6d561cd8','Fundamentals of TypeScript','...', 10, 69.0),
('61d21fdc00988e6ca9284474','Angular for front-end Development',NULL, 30, 99.0),
('61d21fdc505ac83890e2edb0','API´s with NestJS framework',NULL, 3, 89.9),
('61d21fdc68ead7e2525d8326','FullStack Angular/NestJS',NULL, 7, 199.9);