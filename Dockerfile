FROM node:18-slim AS development

WORKDIR /usr/code

COPY package*.json ./

RUN npm install glob rimraf

RUN npm install --only=dev

COPY . .

RUN npm run build

FROM node:18-slim AS production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/code

COPY package*.json ./

RUN npm install --only=prod

COPY . .

COPY --from=development /usr/code/dist ./dist

CMD ["node", "dist/src/main"]