export const ROUTES = {
  courses: 'courses',
  students: 'students',
  auth: 'auth',
  users: 'users',
};
