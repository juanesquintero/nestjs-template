import { AnyClass, json, Native } from './models';

export const classMock = (_class: AnyClass, mock: Native): AnyClass => {
  const methods = Object.getOwnPropertyNames(_class.prototype);
  const jestfn = jest.fn().mockResolvedValue(mock);
  return methods.reduce((accum, value) => ({ ...accum, [value]: jestfn }), {});
};

export const servicerMock = (service: AnyClass, value: Native): AnyClass => {
  const mock = classMock(service, value);
  mock.remove = jest.fn().mockResolvedValue(true);
  mock.findAll = jest.fn().mockResolvedValue([value]);
  return mock;
};

export const isEmpty = (value: any[] | json): boolean => {
  if (Array.isArray(value)) {
    return !(!!value && value.length > 0);
  }
  return !(!!value && Object.keys(value).length !== 0);
};
