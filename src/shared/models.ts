export class DeleteResponse {
  deleted: boolean;
}

export type json = Record<string, any>;

export interface AnyClass {
  [key: string]: any;
}

export type dict = {
  [key: string]: any;
};

export type Native = json | string | boolean | AnyClass[] | json[];
