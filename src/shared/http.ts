import { NotFoundException, ForbiddenException } from '@nestjs/common';
import { isEmpty } from './utils';

export const NotFound = (record: any, msg = '') => {
  if (isEmpty(record)) {
    throw new NotFoundException(msg);
  }
};

export const Forbidden = (msg = '') => {
  throw new ForbiddenException(msg);
};
