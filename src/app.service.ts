import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

@Injectable()
export class AppService {
  constructor(@InjectConnection() private readonly db: Connection) {}

  async findName(): Promise<string> {
    const sql = `SELECT current_database();`;
    const currentDB = await this.db.query(sql);
    return currentDB[0].current_database;
  }
}
