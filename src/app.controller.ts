import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getIndex(): Promise<{ api: string }> {
    const name = await this.appService.findName();
    return { api: name };
  }
}
