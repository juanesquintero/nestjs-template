import { PickType } from '@nestjs/swagger';
import { User } from './../../users/entities/users.entity';

export class LoginDto extends PickType(User, ['email', 'password'] as const) {}
