import { jwtConstants } from './../config/jwt.config';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { compare, hash } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { SingupDto } from './dto/singup.dto';
import { User } from '../users/entities/users.entity';
import { NotFound, Forbidden } from '../shared/http';
import { AccessToken } from './dto/token.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async loginUser(credentials: LoginDto): Promise<AccessToken> {
    const { email, password } = credentials;
    const user = await this.userRepo.findOne({
      where: { email: email },
    });
    NotFound(user, 'USER NOT FOUND');
    const checkPassword = await compare(password, user.password);
    if (!checkPassword) Forbidden('INVALID PASSWORD');
    const payload = {
      subject: {
        email: user.email,
        name: user.name,
      },
    };
    return {
      type: jwtConstants.type,
      accessToken: this.jwtService.sign(payload),
    };
  }

  async registerUser(user: SingupDto) {
    const passwordHashed = await hash(user.password, 10);
    user = { ...user, password: passwordHashed };
    const newUser = this.userRepo.create(user);
    return this.userRepo.save(newUser);
  }
}
