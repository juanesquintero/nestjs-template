import { Repository } from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { loginMock, userMock } from '../mocks/user.mock';
import { User } from './../users/entities/users.entity';
import { classMock } from './../shared/utils';
import { AuthService } from './auth.service';
import { AccessToken } from './dto/token.dto';
import * as bcrypt from 'bcrypt';

jest
  .spyOn(bcrypt, 'compare')
  .mockImplementation((raw: string, hash: string) => Promise.resolve(true));

describe('AuthService', () => {
  let service: AuthService;
  let userRepo: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: classMock(Repository, userMock),
        },
        {
          provide: JwtService,
          useValue: classMock(JwtService, {}),
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    userRepo = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('loginUser()', () => {
    let accessToken: AccessToken;
    beforeEach(async () => {
      accessToken = await service.loginUser(loginMock);
    });
    it(`should call user repo findOne`, () => {
      expect(userRepo.findOne).toHaveBeenLastCalledWith({
        where: { email: loginMock.email },
      });
    });

    it(`should return jwt accessToken`, () => {
      expect(Object.keys(accessToken)).toEqual(['type', 'accessToken']);
    });
  });

  describe('registerUser()', () => {
    it(`should return user created`, async () => {
      const result = await service.registerUser(userMock);
      expect(result).toBe(userMock);
    });
  });
});
