import { Controller, Post, Body, HttpStatus, HttpCode } from '@nestjs/common';
import {
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserDTO } from '../users/dto/users.dto';
import { ROUTES } from './../shared/constants';
import { AuthService } from './auth.service';
import { AccessToken } from './dto/token.dto';
import { LoginDto } from './dto/login.dto';
import { SingupDto } from './dto/singup.dto';

@ApiTags(ROUTES.auth)
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse({ type: AccessToken })
  @ApiUnauthorizedResponse()
  @HttpCode(HttpStatus.OK)
  @Post('login')
  login(@Body() credentials: LoginDto) {
    return this.authService.loginUser(credentials);
  }

  @ApiOkResponse({ type: UserDTO })
  @ApiUnprocessableEntityResponse()
  @HttpCode(HttpStatus.CREATED)
  @Post('singup')
  singup(@Body() user: SingupDto) {
    return this.authService.registerUser(user);
  }

  // @Get('me')
  // me(@JWT) {
  //   return this.authService.me(JWT);
  // }
}
