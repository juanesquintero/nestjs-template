import { servicerMock } from './../shared/utils';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { SingupDto } from './dto/singup.dto';

describe('AuthController', () => {
  let controller: AuthController;
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: AuthService,
          useValue: servicerMock(AuthService, {}),
        },
      ],
      controllers: [AuthController],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('login()', () => {
    it('should call service loginUser', () => {
      controller.login({} as LoginDto);
      expect(service.loginUser).toHaveBeenCalledWith({});
    });
  });

  describe('singup()', () => {
    it('should call service registerUser', () => {
      controller.singup({} as SingupDto);
      expect(service.registerUser).toHaveBeenCalledWith({});
    });
  });
});
