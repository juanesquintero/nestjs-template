import {
  Controller,
  Get,
  Post,
  Put,
  Param,
  Body,
  Delete,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { DeleteResponse } from './../shared/models';
import { ROUTES } from '../shared/constants';
import { UsersService } from './users.service';
import { User } from './entities/users.entity';
import { UserDTO } from './dto/users.dto';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';

@ApiBearerAuth()
@ApiTags(ROUTES.users)
@Controller(ROUTES.users)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOkResponse({ type: UserDTO, isArray: true })
  @ApiNotFoundResponse()
  @UseGuards(JwtAuthGuard)
  @Get()
  async getAll(): Promise<UserDTO[]> {
    return this.usersService.findAll();
  }

  @ApiOkResponse({ type: UserDTO })
  @ApiNotFoundResponse()
  @UseGuards(JwtAuthGuard)
  @Get(':email')
  async getOne(@Param('email') email: string): Promise<UserDTO> {
    return this.usersService.findOne(email);
  }

  @UseGuards(JwtAuthGuard)
  @ApiCreatedResponse({ type: UserDTO })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  async post(@Body() course: User): Promise<UserDTO> {
    return this.usersService.create(course);
  }

  @ApiOkResponse({ type: DeleteResponse })
  @ApiNotFoundResponse()
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':email')
  async delete(@Param('email') email: string): Promise<DeleteResponse> {
    const deletation = await this.usersService.remove(email);
    return { deleted: deletation };
  }

  @ApiOkResponse({ type: UserDTO })
  @ApiNotFoundResponse()
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.CREATED)
  @Put(':email')
  async put(
    @Param('email') email: string,
    @Body() newUser: User,
  ): Promise<UserDTO> {
    return this.usersService.update(email, newUser);
  }
}
function ApiBearerToken() {
  throw new Error('Function not implemented.');
}

