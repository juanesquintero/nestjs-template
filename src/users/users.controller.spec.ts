import { Test, TestingModule } from '@nestjs/testing';
import { servicerMock } from './../shared/utils';
import { UsersService } from '../users/users.service';
import { UsersController } from './users.controller';
import { User } from './entities/users.entity';

const mock = { email: 'email' };
const serviceMock = servicerMock(UsersService, mock);

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: UsersService,
          useValue: serviceMock,
        },
      ],
      controllers: [UsersController],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAll()', () => {
    it('should call service findAll', async () => {
      expect(await controller.getAll()).toStrictEqual([mock]);
      expect(service.findAll).toHaveBeenLastCalledWith();
    });
  });

  describe('getOne()', () => {
    it('should call service findOne with given course email', async () => {
      expect(await controller.getOne(mock.email)).toStrictEqual(mock);
      expect(service.findOne).toHaveBeenLastCalledWith(mock.email);
    });
  });

  describe('post()', () => {
    it('should call service create', async () => {
      expect(await controller.post({} as User)).toStrictEqual(mock);
      expect(service.create).toHaveBeenCalledWith({});
    });
  });

  describe('update()', () => {
    it('should call service update', async () => {
      expect(await controller.put(mock.email, {} as User)).toStrictEqual(mock);
      expect(service.update).toHaveBeenLastCalledWith(mock.email, {});
    });
  });

  describe('delete()', () => {
    it('should call service update', async () => {
      expect(await controller.delete(mock.email)).toStrictEqual({
        deleted: true,
      });
      expect(service.remove).toHaveBeenLastCalledWith(mock.email);
    });
  });
});
