import { Entity, Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { ROUTES } from '../../shared/constants';
import { UserDTO } from '../dto/users.dto';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

@Entity(ROUTES.users)
export class User extends UserDTO {
  @Column({ nullable: false, length: 200 })
  @ApiProperty()
  @MinLength(4)
  @MaxLength(24)
  @IsNotEmpty()
  password!: string;
}
