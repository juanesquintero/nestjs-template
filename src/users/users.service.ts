import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/users.entity';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async findAll(): Promise<User[]> {
    const users = this.userRepo.find();
    if (users) return users;
    throw new NotFoundException();
  }

  async findOne(email: string): Promise<User> {
    const user = this.userRepo.findOne({ where: { email: email } });
    if (user) return user;
    throw new NotFoundException();
  }

  async create(user: User): Promise<User> {
    const newUser = this.userRepo.create(user);
    return this.userRepo.save(newUser);
  }

  async remove(email: string): Promise<boolean> {
    const deleteResult = await this.userRepo.delete(email);
    return deleteResult.affected > 0;
  }

  async update(email: string, newUser: User): Promise<User> {
    const user = await this.userRepo.findOne({ where: { email: email } });
    if (!user) throw new NotFoundException();
    this.userRepo.merge(user, newUser);
    return this.userRepo.save(user);
  }
}
