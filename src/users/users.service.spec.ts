import { classMock } from './../shared/utils';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { User } from './entities/users.entity';
import { Repository } from 'typeorm';

describe('UsersService', () => {
  let service: UsersService;
  let repository: Repository<User>;
  const mock = { email: 'email' };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: classMock(Repository, mock),
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    it('should call repository find', () => {
      service.findAll();
      expect(repository.find).toHaveBeenLastCalledWith();
    });
  });

  describe('findOne()', () => {
    it('should call repository findOne with given user email', () => {
      service.findOne(mock.email);
      expect(repository.findOne).toHaveBeenLastCalledWith({ where: mock });
    });
  });

  describe('create()', () => {
    it('should call repository create & save', () => {
      service.create({} as User);
      expect(repository.create).toHaveBeenCalled();
      expect(repository.save).toHaveBeenCalled();
    });
  });

  describe('update()', () => {
    it('should call repository findOne, merge & save', () => {
      service.update(mock.email, {} as User);
      expect(repository.findOne).toHaveBeenLastCalledWith({ where: mock });
      expect(repository.merge).toHaveBeenCalled();
      expect(repository.save).toHaveBeenCalled();
    });
  });
});
