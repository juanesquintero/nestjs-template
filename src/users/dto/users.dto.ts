import { Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class UserDTO {
  @Column({ primary: true, nullable: false, length: 200 })
  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email!: string;

  @Column({ unique: true, nullable: false, length: 200 })
  @ApiProperty()
  @IsNotEmpty()
  name!: string;

  @Column({ nullable: false, length: 20 })
  @ApiProperty()
  status!: string;
}
