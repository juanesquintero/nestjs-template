import { LoginDto } from '../auth/dto/login.dto';
import { SingupDto } from '../auth/dto/singup.dto';

export const loginMock: LoginDto = {
  email: 'jhon_doe@mail.com',
  password: '1234',
};

export const userMock: SingupDto = {
  ...loginMock,
  name: 'Jhon Doe',
  status: 'active',
};

export const { password, ...userDTOMock } = userMock;
