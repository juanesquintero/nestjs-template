import { NotFound } from './../shared/http';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { Course } from './entities/courses.entity';
import { CourseDTO } from './dto/courses.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course) private courseRepo: Repository<Course>,
  ) {}

  async findAll(): Promise<Course[]> {
    const courses = await this.courseRepo.find();
    NotFound(courses);
    return courses;
  }

  async findOne(id: string): Promise<Course> {
    const course = await this.courseRepo.findOne({ where: { id: id } });
    NotFound(course);
    return course;
  }

  async create(course: CourseDTO): Promise<Course> {
    const newCourse = this.courseRepo.create({ id: uuidv4(), ...course });
    return this.courseRepo.save(newCourse);
  }

  async remove(id: string): Promise<boolean> {
    const deleteResult = await this.courseRepo.delete(id);
    return deleteResult.affected > 0;
  }

  async update(id: string, newCourse: CourseDTO): Promise<Course> {
    const course = await this.courseRepo.findOne({ where: { id: id } });
    NotFound(course);
    this.courseRepo.merge(course, newCourse);
    return this.courseRepo.save(course);
  }
}
