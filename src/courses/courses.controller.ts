import { JwtAuthGuard } from './../auth/jwt/jwt-auth.guard';
import {
  Controller,
  Get,
  Post,
  Put,
  Param,
  Body,
  Delete,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { ROUTES } from '../shared/constants';
import { DeleteResponse } from '../shared/models';
import { CoursesService } from './courses.service';
import { Course } from './entities/courses.entity';
import { CourseDTO } from './dto/courses.dto';

@ApiTags(ROUTES.courses)
@Controller(ROUTES.courses)
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiOkResponse({ type: Course, isArray: true })
  @ApiNotFoundResponse()
  @Get()
  async getAll(): Promise<Course[]> {
    return this.coursesService.findAll();
  }

  @ApiOkResponse({ type: Course })
  @ApiNotFoundResponse()
  @Get(':id')
  async getOne(@Param('id') id: string): Promise<Course> {
    return this.coursesService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiCreatedResponse({ type: Course })
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(JwtAuthGuard)
  @Post()
  async post(@Body() course: CourseDTO): Promise<Course> {
    return this.coursesService.create(course);
  }

  @ApiBearerAuth()
  @ApiOkResponse({ type: DeleteResponse })
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: string): Promise<DeleteResponse> {
    const deletation = await this.coursesService.remove(id);
    return { deleted: deletation };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ type: Course })
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async put(
    @Param('id') id: string,
    @Body() newCourse: CourseDTO,
  ): Promise<Course> {
    return this.coursesService.update(id, newCourse);
  }
}
