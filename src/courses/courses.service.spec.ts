import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { classMock } from './../shared/utils';
import { Course } from './entities/courses.entity';
import { CoursesService } from './courses.service';
import { CourseDTO } from './dto/courses.dto';

jest.useFakeTimers();

describe('CoursesService', () => {
  let service: CoursesService;
  let repository: Repository<Course>;
  const mock = { id: 'id' };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CoursesService,
        {
          provide: getRepositoryToken(Course),
          useValue: classMock(Repository, mock),
        },
      ],
    }).compile();

    service = module.get<CoursesService>(CoursesService);
    repository = module.get<Repository<Course>>(getRepositoryToken(Course));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    it('should call repository find', () => {
      service.findAll();
      expect(repository.find).toHaveBeenLastCalledWith();
    });

    it('should throw NotFound if there is no courses', async () => {
      repository.find = jest.fn().mockResolvedValue([]);
      try {
        await service.findAll();
      } catch (e) {
        expect(e.message).toBe('Not Found');
      }
    });
  });

  describe('findOne()', () => {
    it('should call repository findOne with given course id', () => {
      service.findOne(mock.id);
      expect(repository.findOne).toHaveBeenLastCalledWith({ where: mock });
    });

    it('should throw NotFound if not exists course with that id', async () => {
      repository.findOne = jest.fn().mockResolvedValue(null);
      try {
        await service.findOne(mock.id);
      } catch (e) {
        expect(e.message).toBe('Not Found');
      }
    });
  });

  describe('create()', () => {
    it('should call repository create & save', () => {
      service.create({} as CourseDTO);
      expect(repository.create).toHaveBeenCalled();
      expect(repository.save).toHaveBeenCalled();
    });
  });

  describe('update()', () => {
    it('should call repository findOne, merge & save', () => {
      service.update(mock.id, {} as CourseDTO);
      expect(repository.findOne).toHaveBeenLastCalledWith({ where: mock });
      expect(repository.merge).toHaveBeenCalled();
      expect(repository.save).toHaveBeenCalled();
    });

    it('should throw NotFound if not exists course with that id', async () => {
      repository.findOne = jest.fn().mockResolvedValue(null);
      try {
        await service.update(mock.id, {} as CourseDTO);
      } catch (e) {
        expect(e.message).toBe('Not Found');
      }
    });
  });
});
