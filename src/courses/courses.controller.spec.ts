import { Test, TestingModule } from '@nestjs/testing';
import { servicerMock } from '../shared/utils';
import { CoursesService } from './courses.service';
import { CoursesController } from './courses.controller';
import { CourseDTO } from './dto/courses.dto';
import { json } from '../shared/models';

jest.useFakeTimers();

describe('CoursesController', () => {
  let controller: CoursesController;
  let service: CoursesService;
  const mock: json = { id: 'id' };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CoursesService,
          useValue: servicerMock(CoursesService, mock),
        },
      ],
      controllers: [CoursesController],
    }).compile();

    controller = module.get<CoursesController>(CoursesController);
    service = module.get<CoursesService>(CoursesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAll()', () => {
    it('should call service findAll', () => {
      controller.getAll();
      expect(service.findAll).toHaveBeenLastCalledWith();
    });
  });

  describe('getOne()', () => {
    it('should call service findOne with given course id', () => {
      controller.getOne(mock.id);
      expect(service.findOne).toHaveBeenLastCalledWith(mock.id);
    });
  });

  describe('post()', () => {
    it('should call service create', () => {
      controller.post({} as CourseDTO);
      expect(service.create).toHaveBeenCalledWith({});
    });
  });

  describe('update()', () => {
    it('should call service update', () => {
      controller.put(mock.id, {} as CourseDTO);
      expect(service.update).toHaveBeenLastCalledWith(mock.id, {});
    });
  });
});
