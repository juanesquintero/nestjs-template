import { Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

export class CourseDTO {
  @Column({ unique: true, length: 100 })
  @ApiProperty()
  title: string;

  @Column({ nullable: true, length: 200 })
  @ApiProperty({ required: false })
  description?: string;

  @Column({ type: 'integer' })
  @ApiProperty({ type: 'integer' })
  hours: number;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  @ApiProperty()
  price: number;
}
