import { Entity, Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { ROUTES } from '../../shared/constants';
import { CourseDTO } from '../dto/courses.dto';

@Entity(ROUTES.courses)
export class Course extends CourseDTO {
  @Column({ primary: true, generated: 'uuid' })
  @ApiProperty()
  id: string;
}
