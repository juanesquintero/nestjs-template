import * as dotenv from 'dotenv';
import database from './database.config';

dotenv.config();

export default () => ({
  port: parseInt(process.env.APP_PORT, 10) || 3000,
  db: database,
});
