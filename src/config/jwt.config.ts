import * as dotenv from 'dotenv';
dotenv.config();

export const jwtConstants = {
  secret: process.env.JWT_SECRET,
  expires: '90m',
  type: 'Bearer',
};

export const jwtConfig = {
  secret: jwtConstants.secret,
  signOptions: {
    expiresIn: jwtConstants.expires,
  },
};
