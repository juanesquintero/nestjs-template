import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { types } from 'pg';

types.setTypeParser(types.builtins.NUMERIC, (value: string): number => {
  return parseFloat(value);
});

export const typeOrmModule: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => ({
    type: 'postgres',
    host: configService.get<string>('db.host', 'localhost'),
    port: configService.get<number>('db.port', 5432),
    username: configService.get<string>('db.user', 'postgres'),
    password: configService.get<string>('db.password', 'postgres'),
    database: configService.get<string>('db.name', 'postgres'),
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: false,
    retryDelay: 3000,
    retryAttempts: 10,
    bigNumberStrings: false,
  }),
};
