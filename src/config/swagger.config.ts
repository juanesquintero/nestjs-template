import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ROUTES } from '../shared/constants';

const { courses, students, users, auth } = ROUTES;
const projectDescription =
  `This project contains a scafold template fro NestJS API ` +
  `projects with some special features like (Docker, Swagger,` +
  `PostgreSQL, Interceptors, Pipes, JWT, Auth module, TypeORM)`;

// OpenAPI Swagger Config
const swaggerConfig = new DocumentBuilder()
  .addBearerAuth()
  .setTitle('NestJS Template')
  .setDescription(projectDescription)
  .setVersion('1.0')
  .addTag(courses, `Operations with academic ${courses} entity`)
  .addTag(students, `Operations with ${students} entity for Academy module`)
  .addTag(auth, `Operations for authentication and authorization`)
  .addTag(users, `Operations with ${users} entity for Auth module`)
  .build();

export const setUpSwagger = (app: INestApplication) => {
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);
};
