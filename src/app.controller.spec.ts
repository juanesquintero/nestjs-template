import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
    appService = app.get<AppService>(AppService);
  });

  describe('root - index', () => {
    it('should call findName service', async () => {
      jest.spyOn(appService, 'findName');
      await appController.getIndex();
      expect(appService.findName).toHaveBeenCalled();
    });

    it('should return api name', async () => {
      const response = await appController.getIndex();
      expect(response.api).toBe('academy');
    });
  });
});
