import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { setUpSwagger } from './config/swagger.config';
import { AppModule } from './app.module';

async function bootstrap() {
  // NestJS app
  const app = await NestFactory.create(AppModule, { cors: true });
  // Swagger
  setUpSwagger(app);
  // Validation Pipes
  app.useGlobalPipes(new ValidationPipe());
  // Run app on port config
  const config = app.get(ConfigService);
  await app.listen(config.get('port') || 3000);
}

bootstrap();
