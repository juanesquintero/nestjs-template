import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { AppService } from './app.service';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [AppService],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  describe('findName()', () => {
    it('should call db connection query', async () => {
      const dbConnection = appService['db'];
      jest.spyOn(dbConnection, 'query');
      await appService.findName();
      expect(dbConnection.query).toHaveBeenCalled();
    });

    it('should return api name', async () => {
      const result = await appService.findName();
      expect(result).toBe('academy');
    });
  });
});
