module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '.*\\.spec\\.ts$',
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{js,ts}',
    '!<rootDir>/node_modules/',
    '!<rootDir>/src/main.ts',
    '!<rootDir>/src/config/*.ts',
    '!<rootDir>/src/**/*.config.ts',
    '!<rootDir>/src/shared/constants.ts',
    '!<rootDir>/src/shared/models.ts',
    '!<rootDir>/src/config/*.ts',
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  coverageReporters: ['text', 'html'],
  modulePathIgnorePatterns: [],
};
